**MIDI-EVI**

This is the code for my EVI. It is made with my instrument in mind but I want it to become a modular platform that can be applied to different projects. Please let me know what you think could be improved!

**

---

## Usage

Link of it in action: https://www.instagram.com/p/CJNMXOEA4kh/

List of parts used: TBD

---
